<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagEvent extends Model
{
    protected $guarded = [];

    public function target_user()
    {
        return $this->hasOne(User::class, 'code', 'target');
    }

    public function source_user()
    {
        return $this->hasOne(User::class, 'code', 'code');
    }
}
