<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 30.04.2017
 * Time: 15:02
 */

namespace App\Models\Timetracker;


use Illuminate\Database\Eloquent\Model;

class Task
{
    protected $id;
    protected $name;

    public function __construct($task_obj)
    {
        $this->setId($task_obj->id);
        $this->setName($task_obj->name);

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}