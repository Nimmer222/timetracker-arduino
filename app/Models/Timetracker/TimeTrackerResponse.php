<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 30.04.2017
 * Time: 14:59
 */

namespace App\Models\Timetracker;



use Psr\Http\Message\ResponseInterface;

class TimeTrackerResponse
{
    /**
     * @var Task
     */
    protected $task;

    /**
     * @var Project
     */
    protected $project;

    /**
     * @var TimeRecord
     */
    protected $time_record;

    protected $status_code;

    public function __construct(ResponseInterface $response)
    {
        $body = $response->getBody();
        $content = \GuzzleHttp\json_decode($body->getContents());

        //var_dump($content);

        $this->status_code = $response->getStatusCode();

        if(isset($content->stop_time)) {
            $this->stop_time = $content->stop_time;
        }
        if(isset($content->start_time)) {
            $this->start_time = $content->start_time;
        }
        if(isset($content->plan_table)) {
            $this->plan_table = $content->plan_table;
        }
        if(isset($content->task)) {
            $this->task = new Task($content->task);
        }
        if(isset($content->project)) {
            $this->project = new Project($content->project);
        }
        if(isset($content->time_record)) {
            $this->time_record = new TimeRecord($content->time_record);
        }
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
    }

    /**
     * @return TimeRecord
     */
    public function getTimeRecord()
    {
        return $this->time_record;
    }

    /**
     * @param TimeRecord $time_record
     */
    public function setTimeRecord($time_record)
    {
        $this->time_record = $time_record;
    }



}