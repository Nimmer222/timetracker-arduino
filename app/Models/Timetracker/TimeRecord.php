<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 30.04.2017
 * Time: 15:02
 */

namespace App\Models\Timetracker;


use Illuminate\Database\Eloquent\Model;

class TimeRecord
{
    protected $id;

    public function __construct($project_obj)
    {
        $this->setId($project_obj->id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}