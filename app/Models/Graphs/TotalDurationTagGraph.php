<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 20:11
 */

namespace App\Models\Graphs;


use App\Models\AmCharts\AmChartGraph;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TotalDurationTagGraph extends ArduinoTTGraph
{

    public function initCustomBaseConfig()
    {

        $this->setBaseConfigArray('valueField', $this->value_key);


    }

    public function getData($time_span, $src_user_id = null, $tgt_user_id = null)
    {
        $group_bys = $this->getGroupBys($time_span);
        $group = '';
        $user_cond_and_join = '';

        if(!is_null($src_user_id) && is_null($tgt_user_id)) {
            $user_cond_and_join = ' join users u on te_tmp.code = u.code where u.id = ' . $src_user_id . ' ';
            $group =  ',u.id ';
        }

        if(!is_null($tgt_user_id) && is_null($src_user_id)) {
            $user_cond_and_join = ' join users u on te_tmp.target = u.code where u.id = ' . $tgt_user_id . ' ';
            $group =  ',u.id ';
        }

        $casy =  DB::table('paused_tasks AS pt')
            ->select(DB::raw($group_bys['date_formatting']. ' as date, pt.created_at as created_at, pt.deleted_at as deleted_at'))
            ->join('tag_events AS te', 'te.id', '=', 'pt.tag_event_id')
            ->join(DB::raw('
                  (
                    SELECT te_tmp.id as id, '. $group_bys['group_by_format'] .' as date FROM tag_events te_tmp
                      inner join `paused_tasks` as `pt_tmp` on `te_tmp`.`id` = `pt_tmp`.`tag_event_id`
                    '. $user_cond_and_join . '
          
                    ORDER BY date
                  ) tmp 
                  '), function($join)
            {
                $join->on('te.id', '=', 'tmp.id');
            })
            ->whereNotNull('pt.deleted_at')
            ->orderBy('date');

        var_dump($casy->toSql());
        $casy = $casy->get();

        $result = [];

        foreach ($casy as $item) {
            $start_time = new Carbon($item->created_at);
            $end_time = new Carbon($item->deleted_at);

            if (isset($result[$item->date])){
                $tmp_item = $result[$item->date];
            } else{
                $tmp_item =  new \stdClass();
                $tmp_item->use_count = 0;
            }
            $tmp_item->date = $item->date;
            $tmp_item->use_count += ($end_time->timestamp - $start_time->timestamp);

            $result[$item->date] = $tmp_item;

        }


        return $result;
    }

    private function getGroupBys($time_span)
    {
        $minute_only_format = 'DATE_FORMAT(te_tmp.created_at,\'%Y-%m-%d %H:%i\')';
        $minute_date_formatting = 'CONCAT(DATE_FORMAT(te.created_at,\'%Y-%m-%d %H:%i\'),\':00\')';

        $hour_only_format = 'hour(te_tmp.created_at)';
        $hour_date_formatting = 'CONCAT(DATE_FORMAT(te.created_at,\'%Y-%m-%d %H\'),\'00:00\')';


        $day_only_format = 'day(te_tmp.created_at)';
        $day_date_formatting = 'CONCAT(DATE_FORMAT(te.created_at,\'%Y-%m-%d \'),\'00:00:00\')';

        $month_only_format = 'month(te_tmp.created_at)';
        $month_date_formatting = 'CONCAT(DATE_FORMAT(te.created_at,\'%Y-%m\'),\'-1 00:00:00\')';


        switch ($time_span) {
            case 'minutes':
                $group_by_format = $minute_only_format;
                $date_formatting = $minute_date_formatting;
                break;
            case 'hours':
                $group_by_format = $hour_only_format;
                $date_formatting = $hour_date_formatting;
                break;
            case 'days':
                $group_by_format = $day_only_format;
                $date_formatting = $day_date_formatting;
                break;
            case 'months':
                $group_by_format = $month_only_format;
                $date_formatting = $month_date_formatting;
                break;
            default :
                $group_by_format = $day_only_format;
                $date_formatting = $day_date_formatting;
                break;
        }

        return [
            'group_by_format' => $group_by_format,
            'date_formatting' => $date_formatting
        ];
    }
}