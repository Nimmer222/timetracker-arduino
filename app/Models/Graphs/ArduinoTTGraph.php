<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 30.04.2017
 * Time: 19:20
 */

namespace App\Models\Graphs;


use App\Models\AmCharts\AmChartGraph;

abstract class ArduinoTTGraph extends AmChartGraph
{
    public abstract function getData($time_span, $src_user_id = null, $tgt_user_id = null);
}