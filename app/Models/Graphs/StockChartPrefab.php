<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 19:41
 */

namespace App\Models\Graphs;


use App\Models\AmCharts\AmChartGraph;
use App\Models\AmCharts\AmChartStock;

class StockChartPrefab extends AmChartStock
{
    public function initCustomConfigJson()
    {

        $this->setBaseConfigArray('theme', "light");
        $this->setBaseConfigArray('marginRight', 40);
        $this->setBaseConfigArray('marginLeft', 40);
        $this->setBaseConfigArray('autoMarginOffset', 20);
        $this->setBaseConfigArray('mouseWheelZoomEnabled', true);

        $this->setBaseConfigArray('categoryAxis', [
            "minPeriod" => "ss",
            "parseDates" => true,
            "axisAlpha" => 1,
            "firstDayOfWeek" => 0,
            "markPeriodChange" => true,
            "titleFontSize" => 0
	    ]);

        $this->setBaseConfigArray('dataDateFormat', "YYYY-MM-DD JJ:NN:SS");
        $this->setBaseConfigArray('chartScrollbar', [
            "graph" => "reaction_time",
            "oppositeAxis" =>false,
            "offset" =>30,
            "scrollbarHeight" => 80,
            "backgroundAlpha" => 0,
            "selectedBackgroundAlpha" => 0.1,
            "selectedBackgroundColor" => "#888888",
            "graphFillAlpha" => 0,
            "graphLineAlpha" => 0.5,
            "selectedGraphFillAlpha" => 0,
            "selectedGraphLineAlpha" => 1,
            "autoGridCount" =>true,
            "color" =>"#AAAAAA"
        ]);
        $this->setBaseConfigArray('chartCursor', [
            "enabled" => true,
		    "categoryBalloonDateFormat" => "YYYY-MM-DD JJ:NN:SS"
	    ]);

        $this->setBaseConfigArray('categoryField', "date");
    }

    public function clearGraphs()
    {
        unset($this->base_config_array['graphs']);
    }

    public function addGraph(AmChartGraph $graph)
    {
        $graph_array = $graph->getConfig();
        $this->setBaseConfigArray('graphs', [$graph_array]);
    }
}