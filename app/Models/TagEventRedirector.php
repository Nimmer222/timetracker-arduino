<?php
namespace App\Models;



use App\Models\Timetracker\TimeTrackerResponse;
use App\PausedTasks;
use App\TagEvent;
use App\User;
use GuzzleHttp\Client;

use Psr\Http\Message\ResponseInterface;


class TagEventRedirector
{
    const timetracker_api_url = "dev3.taskman.ui42.sk/timetracker/api/";
    protected static $debug = false;

    /**
     * @param TagEvent $event
     * @return TimeTrackerResponse
     */
    public static function getTargetCurrent(TagEvent $event)
    {

        $target_user = $event->target_user()->getResults();
        $url = url($target_user->timetracker_id . "/get-currently-tracking");
        if (!static::$debug) {
            $url = static::timetracker_api_url . "get-currently-tracking/" . $target_user->timetracker_id  ;
        }


        $client = new Client();
        $response = $client->get($url);
        $tt_response = new TimeTrackerResponse($response);

        return $tt_response;
    }

    public static function startTracking($target_task_id, User $target_user)
    {
        $url = url($target_task_id . "/" . $target_user->timetracker_id . "/start-tracking");
        if (!static::$debug) {
            $url = static::timetracker_api_url .  "start-tracking/" . $target_task_id . "/" . $target_user->timetracker_id;
        }

        $client = new Client();
        $response = $client->get($url);
        $tt_response = new TimeTrackerResponse($response);

        return $tt_response;
    }

    public static function stopTimeRecord($record_id)
    {
        $url = url($record_id . "/stop-tracking");
        if (!static::$debug) {
            $url = static::timetracker_api_url . "stop-tracking/" . $record_id . '?time_value=1&note=';
        }

        $client = new Client();
        $response = $client->get($url);

        $tt_response = new TimeTrackerResponse($response);

        return $tt_response;
    }

    public static function getSourceCurrent(TagEvent $tag_event)
    {
        $target_user = $tag_event->source_user()->getResults();

        $url = url($target_user->timetracker_id . "/get-currently-tracking");
        if (!static::$debug) {
            $url = static::timetracker_api_url .  "get-currently-tracking/" . $target_user->timetracker_id ;
        }

        $client = new Client();
        $response = $client->get($url);
        $tt_response = new TimeTrackerResponse($response);

        return $tt_response;
    }

    public static function storePaused(TagEvent $event, TimeTrackerResponse $response_helper)
    {
        $task = $response_helper->getTask();

        $user = $event->target_user()->getResults();

        PausedTasks::create([
            'tag_event_id' => $event->id,
            'task_id' => $task->getId(),
            'user_id' => $user->id
        ]);
    }

}