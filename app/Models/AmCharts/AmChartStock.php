<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 19:40
 */

namespace App\Models\AmCharts;


class AmChartStock extends AmChartSeries
{
    public function initCustomConfigJson()
    {


        $this->setBaseConfigArray('dataDateFormat', "YYYY-MM-DD HH:NN:SS");

    }
}