<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 19:10
 */

namespace App\Models\AmCharts;


class DataProvider
{
    protected $data;

    public function addDataItem($value, $key = null)
    {
        if (!is_null($key)) {
            $this->data[$key] = $value;
        } else{
                $this->data[] = $value;
        }
    }

    public function getData()
    {
        return $this->data;
    }

}