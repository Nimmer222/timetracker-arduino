<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 19:44
 */

namespace App\Models\AmCharts;


interface AmChartObjectInterface
{
    function getCode();

    function getDataProvider();

    function setDataProvider($provider);
    function setBaseConfigArray($key_name, $config);

}