<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 20:18
 */

namespace App\Models\AmCharts;


abstract class AmChartGraph
{
    protected $base_config_array;
    protected $id;
    protected $value_key = 'value';

    public function __construct($id)
    {
        $this->id = $id;
        $this->initBaseConfig();
    }

    public function setValueIdentifier($value_key){
        $this->value_key = $value_key;
    }

    public function getConfig()
    {
        return $this->getBaseConfigs();
    }

    private function getBaseConfigs()
    {
        return $this->base_config_array;
    }

    public function setBaseConfigArray($key_name, $config)
    {
        $this->base_config_array[$key_name] = $config;
    }

    protected function initBaseConfig()
    {
        $this->setBaseConfigArray('id', $this->id);

        $this->initCustomBaseConfig();
    }

    abstract public function initCustomBaseConfig();
}