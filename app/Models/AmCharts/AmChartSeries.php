<?php

namespace App\Models\AmCharts;

/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 18:29
 */
class AmChartSeries extends AmChartObject
{

    protected $chart_title;

    public function __construct($chart_div_id = "chartdiv", $data_provider = null, $chart_title = 'Chart Title')
    {
        $this->chart_title = $chart_title;
        if (is_null($data_provider)){
            $data_provider = new DataProvider();
        }

        $this->data_provider = $data_provider;
        $this->chart_div_id = $chart_div_id;

        $this->basicInit();
    }

    public function getCode()
    {
        $this->initConfigJson();

        $resultHtml = $this->importScripts();

        $resultHtml .= '<script>';
        $resultHtml .= $this->getChartCode();
        $resultHtml .= '</script>';


        return $resultHtml;
    }

    private function importScripts()
    {

        $scriptsHtml = '';
        $scriptsHtml .= '<script src="' . asset($this->scriptRelPath) . '"></script>';
        $scriptsHtml .= '<script src="' . asset($this->serialScriptRelPath) . '"></script>';

        return $scriptsHtml;
    }

    private function getChartCode()
    {
        $chartCodeArray = $this->getBaseConfigs();

        $chartConfigJson = json_encode($chartCodeArray);

        $chartCode = 'var chart = AmCharts.makeChart(' . $this->chart_div_id . ',' . $chartConfigJson . ');';
        return $chartCode;
    }

    public function setDataProvider($provider)
    {
        $this->data_provider = $provider;
    }

    public function setBaseConfigArray($key_name, $config)
    {
        $this->base_config_array[$key_name] = $config;
    }

    private function getBaseConfigs()
    {
        return $this->base_config_array;
    }


    public function buildTestChart()
    {
        $this->data_provider->addDataItem([
             "category" => "category 1",
             "column-1"=> 8
        ]);

        $this->data_provider->addDataItem([
            "category" => "category 2",
            "column-1"=> 10
        ]);
    }

    public function buildTestRawChart()
    {
        $chartCode = 'var chart = AmCharts.makeChart(' . $this->chart_div_id . ',';
        $chartCode .= '{';

        $chartCode .= '
            "type": "serial",
            "categoryField": "category",
        ';

        $chartCode .= '
        "categoryAxis": {
            "gridPosition": "start",
            "titleFontSize": 0
        },
        ';

        $chartCode .= '
        "graphs": [
            {
                "title": "Graph title",
                "valueField": "column-1"
            }
        ],
        ';

        $chartCode .= '
         "valueAxes": [
            {
                "title": "Axis title"
            }
        ],
        ';

        $chartCode .= '
        "legend": {
            "useGraphSettings": true
        },
        ';

        $chartCode .= '
         "titles": [
            {
                "size": 15,
                "text": "Chart Title"
            }
        ],
        ';

        $chartCode .= '
         "dataProvider": [
            {
                "category": "category 1",
                "column-1": 8
            },
            {
                "category": "category 2",
                "column-1": 10
            },
          ]
        ';
        $chartCode .= '
            });
        ';

        return $chartCode;
    }

    private function initConfigJson()
    {

        $this->initCustomConfigJson();

        $this->setBaseConfigArray('dataProvider', $this->data_provider->getData());
    }


    protected function initCustomConfigJson()
    {

    }

    private function basicInit()
    {
        $this->setBaseConfigArray('type', "serial");

        $this->setBaseConfigArray('categoryField', "category");

        $this->setBaseConfigArray('categoryAxis', [
            "gridPosition" => "start"
        ]);

        $this->setBaseConfigArray('graphs', [
            [
                "title" => "Graph title",
                "valueField" => "column-1"
            ]
        ]);

        $this->setBaseConfigArray('valueAxes', [
            [
                "title" => "Axis title",
                "id" => "v1",
                "axisAlpha" => 0,
                "position" => "left",
                "ignoreAxisWidth" => true,
                "autoWrap" => true
            ]
        ]);

        $this->setBaseConfigArray('legend' , [
            "useGraphSettings" => true
        ]);

        $this->setBaseConfigArray('titles', [
            [
                "size" => 15,
                "text" => $this->chart_title
            ]
        ]);
    }


}