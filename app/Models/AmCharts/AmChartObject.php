<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 15.03.2017
 * Time: 19:55
 */

namespace App\Models\AmCharts;


abstract class AmChartObject implements AmChartObjectInterface
{
    /**
     * @var DataProvider
     */
    protected $data_provider;
    protected $chart_div_id;
    protected $scriptRelPath = 'js/amcharts3/amcharts/amcharts.js';
    protected $serialScriptRelPath = 'js/amcharts3/amcharts/serial.js';
    protected $base_config_array = [];

   protected abstract function initCustomConfigJson();

    function getDataProvider()
    {
        return $this->data_provider;
    }
}