<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 23.03.2017
 * Time: 22:21
 */

namespace App\Http\Controllers;


use App\Models\Graphs\ArduinoTTGraph;
use App\Models\Graphs\DailyTagGraph;
use App\Models\Graphs\StockChartPrefab;

use App\Models\Graphs\UseCountTagGraph;
use App\TagEvent;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class GraphsController extends Controller
{

     public function showGraph($graph_type, $user_id = null)
    {
        $time_span = Input::get('time_span');
        $src_user_id = Input::get('source_user_id');
        $tgt_user_id = Input::get('target_user_id');


        $stockChart = new StockChartPrefab("chartdiv", null,
            config('graphs.' .$graph_type. '.name') .
            " by " . $time_span .
            ((!is_null($src_user_id) || !is_null($tgt_user_id)) ?'(' : '') .
            ($this->isUserId($src_user_id)? 'source_user = '. User::find($src_user_id)->name : '') .
            ($this->isUserId($tgt_user_id) ? 'target_user='. User::find($tgt_user_id)->name : '') .
            ((!is_null($src_user_id) || !is_null($tgt_user_id)) ?')' : ''));
        $stockChart->clearGraphs();

        /**
         * @var $graph ArduinoTTGraph
         */
        $graph_class = config('graphs.' . $graph_type . '.class');

        $graph = new $graph_class("code_id");
        $graph->setValueIdentifier('use_count');
        $graph->initCustomBaseConfig();
        $stockChart->addGraph($graph);

        foreach ($graph->getData($time_span, $src_user_id, $tgt_user_id) as $item) {
            $stockChart->getDataProvider()->addDataItem($item);
        }

        return view('graphs.basic', ['stock_chart' => $stockChart]);
    }

    private function isUserId($src_user_id)
    {
        return (!is_null($src_user_id) && $src_user_id != -1);
    }


}