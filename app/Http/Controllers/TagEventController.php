<?php

namespace App\Http\Controllers;

use App\Models\TagEventRedirector;
use App\Models\Timetracker\TimeTrackerResponse;
use App\PausedTasks;
use App\TagEvent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TagEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'userTagged']);
    }
    public function index()
    {
        $tag_events = TagEvent::all();

        return view('tag-events.index', ['tag_events' => $tag_events]);
    }
    public function userTagged(Request $request)
    {
        $code = Input::get('code');
        $target = Input::get('target');

        error_log(print_r(Input::all(), true));

        if(!is_null($code)){
            $tag_event = TagEvent::create(['code' => $code, 'target' => $target]);
        }

        if ($this->existsPaused($tag_event->target_user()->getResults())){
            $this->restorePaused($tag_event);
        } else {
            $this->getCurrentAndSwitch($tag_event);
        }

        return;
    }

    private function existsPaused($target_user)
    {
        $paused_task = PausedTasks::where('user_id', '=', $target_user->id)->first();

        return !is_null($paused_task);
    }

    private function getCurrentAndSwitch(TagEvent $tag_event)
    {
        // get current task of target person
        $tgt_current_response = TagEventRedirector::getTargetCurrent($tag_event);

        // stop and save the task
        $old_task_id = $tgt_current_response->getTask()->getId();
        $old_record_id = $tgt_current_response->getTimeRecord()->getId();
        TagEventRedirector::storePaused($tag_event, $tgt_current_response);
        $stopped_response = TagEventRedirector::stopTimeRecord($old_record_id);

        //start tracking on task of the source person
        // in timetracker instance of the target person
        $src_current_response = TagEventRedirector::getSourceCurrent($tag_event);

        $target_user = $tag_event->target_user()->getResults();
        $target_task_id = $src_current_response->getTask()->getId();

        $target_start_response = TagEventRedirector::startTracking($target_task_id, $target_user);

    }

    private function restorePaused(TagEvent $tag_event)
    {
        $current_response = TagEventRedirector::getTargetCurrent($tag_event);

        // stop the running task of target person
        $old_task_id = $current_response->getTask()->getId();
        $old_record_id = $current_response->getTimeRecord()->getId();
        if ($old_record_id != null) {
            $stopped_response = TagEventRedirector::stopTimeRecord($old_record_id);

            // restore the old task the target person was running before interruption
        }
        $target_user = $tag_event->target_user()->getResults();
        /**
         * @var $paused_task Model
         */
        $paused_task = PausedTasks::where('user_id', '=', $target_user->id)
            ->first();

        $target_task_id = $paused_task->task_id;

        $target_start_response = TagEventRedirector::startTracking($target_task_id, $target_user);

        if ($target_start_response->getStatusCode() == '200') {
            $paused_task->delete();
        } else{
            var_dump($target_start_response);
        }
    }

}
