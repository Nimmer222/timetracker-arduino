<?php
/**
 * Created by PhpStorm.
 * User: Matúš Demko
 * Date: 24.03.2017
 * Time: 0:27
 */

namespace App\Http\Controllers;


use App\TagEvent;
use Carbon\Carbon;

class TimetrackerDummyController extends Controller
{
    public function stopTracking($id)
    {
        $current_time = Carbon::create();

        //$time_record = TimeRecord::find($id);

        return response()
            ->json([
                'stop_time' => (isset($current_time) ? $current_time : null),
                'plan_table' => 'asd',
            ]);
    }

    public function startTracking($task_id, $user_id)
    {
        $current_time = Carbon::create();

        return response()
            ->json([
                'start_time' => (isset($current_time) ? $current_time : null),
                'time_record' => [
                    'id' => 100,
                ]
            ]);
    }

    public function getCurrentlyTracking($user_id)
    {

        if ($user_id == 1){
            $time_record_id = 100;
        } else{
            $time_record_id = 200;
        }

        return response()
            ->json([
                'time_record' => [
                    'id' => $time_record_id
                ],
                'task' => [
                    'id' => 132123,
                    'name' => 'Implementovanie API pre arduino',
                ],
                'project' => [
                    'id' => 123,
                    'name' => 'Timetracker/arduino',
                ]
            ]);
    }

    public function restoreTask($old_id){
        // TODO iba zavolat switchTask s idckom stareho tasku
        TagEvent::create(['code' => 'restore', 'target' => $old_id]); exit;
    }

}