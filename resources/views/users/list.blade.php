@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile of {{ $user->name }}</div>


                    <div class="panel-body">

                        @foreach($users as $user)
                            <div>
                                <div>
                                    {{ $user->name }}
                                </div>
                                <div>
                                    {{ $user->code }}
                                </div>
                                <div>
                                    {{ $user->timetracker_id }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection