@extends('layouts.app')

@section('content')
    <div class="content container">
    <h1>Users</h1>

    @foreach($users as $user)
        <div class="row">
            <div class="col-xs-2">
                {{ $user->name }}
            </div>

            <div class="col-xs-2">
                {{ $user->code }}
            </div>
        </div>
    @endforeach
    </div>
@endsection
