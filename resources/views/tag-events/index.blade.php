@extends('layouts.app')

@section('content')
    <div class="content container">
    <h1>Tag events</h1>

    @foreach($tag_events as $tag_event)
        <div class="row">
            <div class="col-xs-2">
                {{ $tag_event->code }}
            </div>

            <div class="col-xs-2">
                {{ $tag_event->target }}
            </div>
        </div>
    @endforeach
    </div>
@endsection
