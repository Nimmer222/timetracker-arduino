@extends('layouts.app')

@section('content')
            <div class="content container">
                <div>
                    <a href="{{url('/tagevents')}}">Tag Events</a>
                </div>
                <div>
                    <a href="{{url('/users')}}">Users</a>
                </div>
                <div>
                    <div class="panel panel-default">
                        <h2 class="panel-heading">
                            Graphs overall
                        </h2>
                        <div class="panel-body">
                            @foreach($graphs as $graph_name => $graph_config)
                                @if($graph_config['can_be_generic'])
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div>
                                                <h3>Graph - {{ $graph_config['name']  }}</h3>
                                            </div>

                                            <div>
                                                <?php
                                                $query_opts = [
                                                        'time_span' => 'minutes',
                                                ];
                                                if(isset($graph_config['user_specifier_default'])) {
                                                    $query_opts[$graph_config['user_specifier']] = $graph_config['user_specifier_default'];
                                                }
                                                ?>
                                                <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query($query_opts))}}"> by minute</a>
                                            </div>
                                            <div>
                                                <?php
                                                $query_opts = [
                                                        'time_span' => 'hours',
                                                ];
                                                if(isset($graph_config['user_specifier_default'])) {
                                                    $query_opts[$graph_config['user_specifier']] = $graph_config['user_specifier_default'];
                                                }
                                                ?>
                                                <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query($query_opts))}}">by hour</a>
                                            </div>
                                            <div>
                                                <?php
                                                $query_opts = [
                                                        'time_span' => 'days',
                                                ];
                                                if(isset($graph_config['user_specifier_default'])) {
                                                    $query_opts[$graph_config['user_specifier']] = $graph_config['user_specifier_default'];
                                                }
                                                ?>
                                                <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query($query_opts))}}">by day</a>
                                            </div>
                                            <div>
                                                <?php
                                                $query_opts = [
                                                        'time_span' => 'months',
                                                ];
                                                if(isset($graph_config['user_specifier_default'])) {
                                                    $query_opts[$graph_config['user_specifier']] = $graph_config['user_specifier_default'];
                                                }
                                                ?>
                                                <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query($query_opts))}}">by month</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                @if(!is_null(\Illuminate\Support\Facades\Auth::user()))
                <div>
                    <div class="panel panel-default">
                        <h2 class="panel-heading">
                            Graphs for you
                        </h2>
                        <div class="panel-body">
                            @foreach($graphs as $graph_name => $graph_config)
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div>
                                            <h3>Graph - {{ $graph_config['name']  }}</h3>
                                        </div>
                                        <div>
                                            <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query([
                                                'time_span' => 'minutes',
                                                 $graph_config['user_specifier'] => \Illuminate\Support\Facades\Auth::user()->id
                                             ]))}}"> by minute</a>
                                        </div>
                                        <div>
                                            <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query([
                                                'time_span' => 'hours',
                                                 $graph_config['user_specifier'] => \Illuminate\Support\Facades\Auth::user()->id
                                             ]))}}">by hour</a>
                                        </div>
                                        <div>
                                            <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query([
                                                'time_span' => 'days',
                                                 $graph_config['user_specifier'] => \Illuminate\Support\Facades\Auth::user()->id
                                             ]))}}">by day</a>
                                        </div>
                                        <div>
                                            <a href="{{url('/graphs/'. $graph_name . '?' . http_build_query([
                                                'time_span' => 'months',
                                                 $graph_config['user_specifier'] => \Illuminate\Support\Facades\Auth::user()->id
                                             ]))}}">by month</a>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>

@endsection