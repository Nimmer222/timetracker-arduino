<?php
return [
    'use_count' => [
        'name' => 'Tag usage count',
        'class' => \App\Models\Graphs\UseCountTagGraph::class,
        'user_specifier' => 'source_user_id',
        'can_be_generic' => true
    ],
    'interrupted_count' => [
        'name' => 'Got interrupted total count',
        'class' => \App\Models\Graphs\UseCountTagGraph::class,
        'user_specifier' => 'target_user_id',
        'can_be_generic' => false
    ],

    'interrupted_people_count' => [
        'name' => 'Tag usage (people needed help count)',
        'class' => \App\Models\Graphs\InterruptedPeopleCount::class,
        'user_specifier' => 'source_user_id',
        'can_be_generic' => true,
        'user_specifier_default' => -1
    ],
    'interrupted_by_people_count' => [
        'name' => 'Interrupted by (people were helpers count)',
        'class' => \App\Models\Graphs\InterruptedPeopleCount::class,
        'user_specifier' => 'target_user_id',
        'can_be_generic' => true,
        'user_specifier_default' => -1
    ],

    'usage_duration' => [
        'name' => 'Usage duration',
        'class' => \App\Models\Graphs\TotalDurationTagGraph::class,
        'user_specifier' => 'source_user_id',
        'can_be_generic' => true
    ],
    'interruption_duration' => [
        'name' => 'Interruption duration',
        'class' => \App\Models\Graphs\TotalDurationTagGraph::class,
        'user_specifier' => 'target_user_id',
        'can_be_generic' => false
    ],
];