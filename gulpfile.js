elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.copy('node_modules/amcharts3', 'public/js/amcharts3');
});