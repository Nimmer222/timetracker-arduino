<?php

Route::post('/arduino/tagged','TagEventController@userTagged');



/**
 * Dummy routes
 */
Route::get('/{id}/stop-tracking','TimetrackerDummyController@stopTracking');
Route::get('/{task_id}/{user_id}/start-tracking','TimetrackerDummyController@startTracking');
Route::get('/{user_id}/get-currently-tracking','TimetrackerDummyController@getCurrentlyTracking');
