<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['graphs' => config('graphs')]);
});



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/graphs/{graph_type}/{user_id?}', 'GraphsController@showGraph');

Route::get('/tagevents', 'TagEventController@index');
Route::get('/users', 'HomeController@indexUsers');

Route::resource('user', 'UserController', [
    'only' => ['show', 'index']
]);


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

             

// Keep this route as the last one
Route::get('/{slug}', '\TCG\Voyager\Http\Controllers\DynamicRouteController@handle');
            